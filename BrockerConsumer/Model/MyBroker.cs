﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Broker.Model
{
    class MyBroker
    {
        ConnectionFactory factory;
        IConnection connection;
        IModel channel;
        string brokerKey = "scrum";
        private  IDataModel dataModel;
        public MyBroker(IDataModel dataModel)
        {
            this.dataModel = dataModel;
            factory = new ConnectionFactory()
            {
                HostName = "shrimp-01.rmq.cloudamqp.com",
                VirtualHost = "zcznxmcl",
                UserName = "zcznxmcl",
                Password = "2oHmuSMadfcxJviP-8stPNdlRh4ftIxb"
            };

            connection = factory.CreateConnection();
            channel = connection.CreateModel();

            channel.QueueDeclare(queue: brokerKey,
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);
        }

        public void ExcecuteSend(string text)
        {
            var body = Encoding.UTF8.GetBytes(text);

            channel.BasicPublish(exchange: "",
                                    routingKey: brokerKey,
                                    basicProperties: null,
                                    body: body);
        }

        public void ExcecuteRead()
        {
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                Console.WriteLine("Message Get" + ea.DeliveryTag);
                var body = ea.Body;
                var message = Encoding.UTF8.GetString(body);
                bool result = dataModel.ExcecuteRead(message);
                if (result)
                   channel.BasicAck(ea.DeliveryTag, true);
                else
                   channel.BasicReject(ea.DeliveryTag, true);
            };
            channel.BasicConsume(queue: brokerKey,
                                 autoAck: false,
                                 consumer: consumer);
        }
    }
}
