﻿using Broker.Model;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace BrockerConsumer
{
    class Program
    {
        static void Main(string[] args)
        {
            IDataModel dataModel = new UserDataModel();
            var myBroker = new MyBroker(dataModel);
            myBroker.ExcecuteRead();           
        }
    }
}
