﻿using Broker.Model;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BrokerProducer
{
    class Program
    {
        static void Main(string[] args)
        {
            IDataModel dataModel = new UserDataModel();
            var myBroker = new MyBroker(dataModel);
            var rnd = new Random();            
            while (true)
            {
                int rndValue = rnd.Next(20, 30);
                int sleep = rndValue * 50;

                User user = new User(rndValue);
                string message = JsonConvert.SerializeObject(user);

                myBroker.ExcecuteSend(message);
                Console.WriteLine($"sleep={sleep}; message={message}");
                Thread.Sleep(sleep);
            }
        }
    }
}
