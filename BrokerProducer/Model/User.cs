﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Broker.Model
{
    class User
    {
        public string name { get; set; }
        public int age { get; set; }
        public string email{ get; set; }

        public User(string name, int age, string email)
        {
            this.name = name;
            this.age = age;
            this.email = email;
        }
        public User(int rndValue)
        {
            this.name = "Name" + rndValue.ToString();
            this.age = rndValue;
            this.email = rndValue % 3 != 0 ? DateTime.Now.ToString("HHmmss") + "@mail.ru" : ""; ;
        }
    }
}
