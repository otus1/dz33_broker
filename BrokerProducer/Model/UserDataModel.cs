﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Broker.Model
{
    public class UserDataModel: IDataModel
    {
        public bool  ExcecuteRead(string text)
        {             
            User user = JsonConvert.DeserializeObject<User>(text);
            Console.WriteLine($"Name = {user.name}; Age={user.age}; Email={user.email}");
            return user.email != "";
        }

    }
}
